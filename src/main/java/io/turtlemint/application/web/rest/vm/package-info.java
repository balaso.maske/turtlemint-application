/**
 * View Models used by Spring MVC REST controllers.
 */
package io.turtlemint.application.web.rest.vm;
