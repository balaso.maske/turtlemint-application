/**
 * Spring Data Elasticsearch repositories.
 */
package io.turtlemint.application.repository.search;
