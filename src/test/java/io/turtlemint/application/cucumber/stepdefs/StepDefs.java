package io.turtlemint.application.cucumber.stepdefs;

import io.turtlemint.application.TurtlemintApplicationApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = TurtlemintApplicationApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
